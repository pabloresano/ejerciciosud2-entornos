package Practica;

import java.util.Scanner;

public class Ejercicio2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		/*Establecer un breakpoint en la primera instruccion y avanzar
		 * instruccion a instruccion (step into) analizando el contenido de las variables
		 */
		
		
		Scanner lector;
		int numeroLeido;
		int resultadoDivision;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un numero");
		numeroLeido = lector.nextInt();

		//Este c�digo da error porque al dividir el numeroleido entre 0, la soluci�n es infinito
		//La soluci�n ser�a cambiar dentro del bucle el signo de igual al valor 0, para que solo llegue al valor 1
		
		//Otra forma de hacer el c�digo mas correcto ser�a  poner la variable resultado
		//division y numeroLeido como double para que el resultado de la divisi�n sea mas exacto
		
		for(int i = numeroLeido; i >= 0 ; i--){
			resultadoDivision = numeroLeido / i;
			System.out.println("el resultado de la division es: " + resultadoDivision);
		}
				
		lector.close();
	}

}
